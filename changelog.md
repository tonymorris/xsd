0.6.2 (2021)
  * Add more string-like instances.

0.6.1 (2021)
  * Add classy optics.

0.6.0 (2021)
  * Refactoring.
  * Update version bounds.

0.5.0 (2014-0214)
  * Refactoring
  * Introduces lenses, isomorphisms and prisms.
  * Dependencies have minimum version bounds.
  * Tests are implemented using doctest.

0.4.0 (2013-0809)
  * Major refactoring by rycee (not backwards-compatible)
